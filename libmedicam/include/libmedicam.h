#if defined (__cplusplus)
extern "C" {
#endif

#ifndef __LIB_MEDICAM_H
#define __LIB_MEDICAM_H

#define RC_OK 0

typedef enum FIELD_OF_VIEW {FOV_NARROW, FOV_MIDDLE, FOV_WIDE} FIELD_OF_VIEW;
typedef enum LED_BRIGHTNESS {LED_OFF, LED_LOW, LED_MIDDLE, LED_HIGH} LED_BRIGHTNESS;
typedef enum WHITE_BALANCE {WB_AUTO = 1, WB_DAYLIGHT, WB_CLOUDY, WB_FLUORESCENT, WB_TUNGSTEN, WB_UNDERWATER} WHITE_BALANCE;

#define EV_MAX 12
#define EV_MIN 0
#define EV_DEFAULT 6

int connectCamera(const char* ipAddress);
void disconnect();

void startRecording();
void stopRecording();
void takeSnapshot();
void checkEvents();

FIELD_OF_VIEW getFov();
void switchFov(FIELD_OF_VIEW fov);
LED_BRIGHTNESS getLedBrightness();
void setLedBrightness(LED_BRIGHTNESS level);
WHITE_BALANCE getWhiteBalance();
void setWhiteBalance(WHITE_BALANCE wb);
void setExposureValue(int value);
void resetExposureValue();

// define event codes
#define EVENT_RECORDING_STARTED 1
#define EVENT_RECORDING_STOPPED 2
#define EVENT_OBJECT_ADDED 3

// Callback function type
typedef void (*event_callback_t)(const int eventCode);

// Set callback function
int setEventCallback(event_callback_t callback);

const char* getLibraryTitle();

#endif // __LIB_MEDICAM_H

#if defined (__cplusplus)
extern "C" {
#endif
