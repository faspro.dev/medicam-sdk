# Fasmedo MediCam SDK

## libmedicam

MediCam remote control library, with shared libraries and sample codes ready to run in Visual Studio Code.

Supported platforms:
* Linux x86_64
* Linux ARM gnueabi

Supported functions:
* Start / stop video recording
* Take video snapshot
* Switch field-of-views
* Switch LED light
* Switch white balance
* Adjust exposure value

Supported events:
* Recording started
* Recording stopped
* Photo or video object added
