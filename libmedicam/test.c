#include <stdio.h>
#include <unistd.h>

#ifdef _WIN32
#include <Windows.h>
#endif
#include "include/libmedicam.h"

static void cameraEventCallback(const int eventCode)
{
    switch (eventCode) {
        case EVENT_RECORDING_STARTED:
            printf("EVENT: recording started\n");
            break;
        case EVENT_RECORDING_STOPPED:
            printf("EVENT: recording stopped\n");
            break;
        case EVENT_OBJECT_ADDED:
            printf("EVENT: photo or video captured\n");
            break;
    }
}

int main()
{
    printf("Library Title: %s\r\n", getLibraryTitle());
    setEventCallback(cameraEventCallback);
    
    int rc = connectCamera("192.168.100.1");
    if (rc == RC_OK)
    {
        resetExposureValue();
        takeSnapshot();
        startRecording();
        stopRecording();

        printf("LED brightness level = %d\n", getLedBrightness());
        printf("White balance index = %d\n", getWhiteBalance());
        FIELD_OF_VIEW fov = getFov();
        printf("Field-of-view: %s\n", fov == FOV_NARROW ? "Narrow" : fov == FOV_MIDDLE ? "Middle" : "Wide");

        for (int level = LED_OFF; level <= LED_HIGH; level++)
        {
            setLedBrightness(level);
#ifdef _WIN32
            Sleep(1);
#else
            sleep(1);
#endif
        }
        setLedBrightness(LED_OFF);

        for (int wb = WB_AUTO; wb <= WB_UNDERWATER; wb++)
        {
            setWhiteBalance(wb);
#ifdef _WIN32
            Sleep(1);
#else
            sleep(1);
#endif
        }

        for (int ev = EV_MIN; ev <= EV_MAX; ev++)
        {
            setExposureValue(ev);
#ifdef _WIN32
            Sleep(1);
#else
            sleep(1);
#endif
        }

        for (int fov = FOV_NARROW; fov <= FOV_WIDE; fov++)
        {
            switchFov(fov);
#ifdef _WIN32
            Sleep(100);
#else
            sleep(10);
#endif // will take effect after stream reloaded
            fov = getFov();
            printf("Field-of-view: %s\n", fov == FOV_NARROW ? "Narrow" : fov == FOV_MIDDLE ? "Middle" : "Wide");
        }
    }
    else
    {
        printf("Connection Error\r\n");
    }
    printf("bye");
    disconnect();
    return 0;
}